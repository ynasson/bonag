<head>
<meta charset="utf-8">
<title>BagHotel</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="shortcut icon" href="favicon.ico">

<!-- Stylesheets -->
<?php 
css_media('animate');
css_media('bootstrap');
css_media('font-awesome.min');
css_media('owl.carouse');
css_media('owl.theme');
css_media('prettyPhot');
css_media('smoothness/jquery-ui-1.10.4.custom.min');
css_media('theme');
css_media('responsive');


?>

<link rel="stylesheet" href="rs-plugin/css/settings.css">
<link rel="stylesheet" href="css/colors/turquoise.css" id="switch_style">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600,700">
<?php

 js_media('jquery-1.11.0.min');
js_media('bootstrap.min');
js_media('bootstrap-hover-dropdown.min');
js_media('owl.carousel.min');
js_media('jquery.parallax-1.1.3');
js_media('jquery.nicescroll');
js_media('jquery.prettyPhoto');
js_media('jquery-ui-1.10.4.custom.min');
js_media('jquery.jigowatt');
js_media('jquery.sticky');
js_media('waypoints.min');
js_media('jquery.isotope.min');
 js_media('jquery.gmap.min');
 ?>
<!-- Javascripts --> 

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script> 
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<?php 
 js_media('switch');
 js_media('custom');
 ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50960990-1', 'slashdown.nl');
  ga('send', 'pageview');
</script>
</head>
